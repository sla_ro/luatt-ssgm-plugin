#ifndef loadlib_h
#define loadlib_h

#define LOADLIB_VERSION     "Loadlib 1.0"
#define LOADLIB_COPYRIGHT   "Copyright (C) 1996-1999 TeCGraf"
#define LOADLIB_AUTHOR      "R. Borges" 

void loadlib_open(void);

#endif

