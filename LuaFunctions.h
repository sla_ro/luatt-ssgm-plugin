/* Lua functions header
by Stan "sla.ro" Laurentiu Alexandru
Copyright 2010-2022 Sla Studios (http://slastudios.net)

This file is part of the LuaTT
*/

#ifndef INCLUDE_LUAFUNCTIONS
#define INCLUDE_LUAFUNCTIONS
#include "LuaLib/lua.hpp"

void AddFunctions(lua_State *L);

//class SpawnerClass;
//extern REF_DECL(DynamicVectorClass<SpawnerClass*>, SpawnerList);

#endif