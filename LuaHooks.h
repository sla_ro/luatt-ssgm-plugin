/*	Lua hooks header
by Stan "sla.ro" Laurentiu Alexandru
Copyright 2010-2022 Sla Studios (http://slastudios.net)

This file is part of the LuaTT
*/

#ifndef INCLUDE_LUAHOOKS
#define INCLUDE_LUAHOOKS
void main_loop_glue();
#endif